﻿    using Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    class StockInitilizer : DropCreateDatabaseIfModelChanges<Context>
    {
        protected override void Seed(Context context)
        {
            #region Districts-מחוז

            var Districts = new List<District>()
            {
            new District() {Name="מרכז"},
            new District() {Name="צפון"},
            new District() {Name="דרום"},
            new District() {Name="ירושלים"},
            new District() {Name="יהודה ושומרון"},
            new District() {Name="חוף"},
            new District() {Name="דן"},
            new District() {Name="שפלה"},
            new District() {Name="שפלה"}
            };
            Districts.ForEach(x => context.Districts.Add(x));
            context.SaveChanges();
            #endregion
            #region Branches-סניף

            var Branches = new List<Branch>()
            {
            new Branch() {Name="מרום ירושלים מרכז",IdDistrict=4,IdManage=4},
            new Branch() {Name="מבשרת והסביבה",IdDistrict=4},
            new Branch() {Name="רמתיים ירושלים צפון",IdDistrict=4},
            new Branch() {Name="ציון ירושלים מרכז",IdDistrict=4},
            new Branch() {Name="מזרח ירושלים",IdDistrict=4},
            new Branch() {Name="ירושלים",IdDistrict=4},

            };
            Branches.ForEach(x => context.Branches.Add(x));
            context.SaveChanges();
            #endregion
            #region Staff-צוות

            var Staffs = new List<Staff>()
            {
            new Staff() {Name="לב העיר",IdBranch=1},
            new Staff() {Name="רובע",IdBranch=1},
            new Staff() {Name="דרום",IdBranch=1},
            new Staff() {Name="מרכז העיר",IdBranch=1},
            new Staff() {Name="גילה הר חומה",IdBranch=1},

            };
            Staffs.ForEach(x => context.Staffes.Add(x));
            context.SaveChanges();
            #endregion
            #region Cities

            var Cities = new List<City>()
            {
            new City() {Name="ירושלים"},
            new City() {Name="בני ברק"},
            new City() {Name="תל אביב"},
            new City() {Name="באר שבע"},
            new City() {Name="אלעד"},

            };
            Cities.ForEach(x => context.Cities.Add(x));
            context.SaveChanges();
            #endregion
            #region Ambulances

            var Ambulances = new List<Ambulance>()
            {
            new Ambulance() {ICU=true,IdCity=1,CodeAmbulance=45,IsActive=true,UserName="111",Password="111"},
            new Ambulance() {ICU=false,IdCity=1,CodeAmbulance=55,IsActive=true,UserName="222",Password="222"},
            new Ambulance() {ICU=false,IdCity=1,CodeAmbulance=65,IsActive=true,UserName="333",Password="333"}
            };
            Ambulances.ForEach(x => context.Ambulances.Add(x));
            context.SaveChanges();
            #endregion          
            #region Trainings

            var Trainings = new List<Training>()
            {
            new Training() {Name=eTrainigNames.חובש_als},
            new Training() {Name=eTrainigNames.נהג_נטן},
            new Training() {Name=eTrainigNames.פראמדיק},
            new Training() {Name=eTrainigNames.רופא},
            new Training() {Name=eTrainigNames.נהג},
            new Training() {Name=eTrainigNames.חובש}
            };
            Trainings.ForEach(x => context.Trainings.Add(x));
            context.SaveChanges();
            #endregion
            #region Permissions

            var Permissions = new List<Permission>()
            {
            new Permission() {Name=ePermissionNames.אחראי_שיבוץ_אזורי},
            new Permission() {Name=ePermissionNames.מנהל_אבטחות_אירועים},
            new Permission() {Name=ePermissionNames.מנהל_אזורי},
            new Permission() {Name=ePermissionNames.מנהל_ארצי},
            new Permission() {Name=ePermissionNames.מנהל_מערכת},
            new Permission() {Name=ePermissionNames.מנהל_מרכז_ציוד_אזורי},
            new Permission() {Name=ePermissionNames.מנהל_מרכז_ציוד_ארצי},
            new Permission() {Name=ePermissionNames.ראש_סניף},
            };
            Permissions.ForEach(x => context.Permissions.Add(x));
            context.SaveChanges();
            #endregion
            #region Volunteers

            Trainings.Where(x => x.Name == eTrainigNames.חובש_als || x.Name == eTrainigNames.נהג_נטן).ToList();

            var Volunteers = new List<Volunteer>()
            {
              new Volunteer() {Email="9197co@gmail.com",FirstName="חיהלה",LastName="אוסטרי",Password="444",
                UserName ="444",IsActive=true,IdStaff=1,Phone="0527629197",PrivateId=12,Tz="208556050",
                Trainings =Trainings.Where(x => x.Name == eTrainigNames.חובש_als|| x.Name == eTrainigNames.נהג_נטן).ToList(),
               Permissions =Permissions.Where(x => x.Name == ePermissionNames.מנהל_ארצי).ToList()},

              new Volunteer() {Email="9197co@gmail.com",FirstName="רבקי",LastName="בלוי",Password="555",
                UserName ="555",IsActive=true,IdStaff=1,Phone="0527638998",PrivateId=22,Tz="308556050",
                Trainings =Trainings.Where(x => x.Name == eTrainigNames.פראמדיק).ToList(),
                Permissions =Permissions.Where(x => x.Name == ePermissionNames. ראש_סניף ||  x.Name == ePermissionNames.מנהל_מערכת).ToList()}

            };
            Volunteers.ForEach(x => context.Volunteers.Add(x));
            context.SaveChanges();
            context.Volunteers.FirstOrDefault(x => x.Id == 4).Trainings.Add(Trainings.First(x => x.Name == eTrainigNames.פראמדיק));
            context.SaveChanges();
            #endregion
            #region ShiftKinds

            var ShiftKinds = new List<ShiftKind>()
            {
            new ShiftKind() {Name="בוקר",EndHour=new TimeSpan(1,0,0),StartHour=new TimeSpan(7,0,0)},
            new ShiftKind() {Name="צהרים",EndHour=new TimeSpan(7,0,0),StartHour=new TimeSpan(1,0,0)},
            new ShiftKind() {Name="ערב",EndHour=new TimeSpan(1,0,0),StartHour=new TimeSpan(7,0,0)},
            new ShiftKind() {Name="לילה",EndHour=new TimeSpan(7,0,0),StartHour=new TimeSpan(1,0,0)},

            };
            ShiftKinds.ForEach(x => context.ShiftKinds.Add(x));
            context.SaveChanges();
            #endregion
            #region Shifts

            var Shifts = new List<Shift>()
            {
            new Shift() {IdAmbulance=1,Date=new DateTime(2019,4,15),IdShiftKind =1},
            new Shift() {IdAmbulance=2,Date=new DateTime(2019,4,15),IdShiftKind =1},
            new Shift() {IdAmbulance=3,Date=new DateTime(2019,3,15),IdShiftKind =1}

            };
            Shifts.ForEach(x => context.Shifts.Add(x));
            context.SaveChanges();

            #endregion
            #region AssignmentsForShift

            var AssignmentsForShifts = new List<AssignmentForShift>()
            {
            new AssignmentForShift() {IdShift=1,IdVolunteer=5,IdTraining=1},
            new AssignmentForShift() {IdShift=1,IdVolunteer=4,IdTraining=3},
            new AssignmentForShift() {IdShift=2,IdVolunteer=4,IdTraining=2}

            };
            AssignmentsForShifts.ForEach(x => context.AssignmentsForShift.Add(x));
            context.SaveChanges();
            #endregion
            #region event

            var Event = new List<Event>()
            {
                new Event() {StartHour=new TimeSpan(12,0,0),EndHour=new TimeSpan(17,0,0), Address=new Address() { City="ירושלים",Number=1,Street="jkjk"},Date=new DateTime(2019,04,25),Name="הפגנה"
               ,Ambulances=Ambulances.Where(x=>x.Id==1||x.Id==2).ToList()},
                 new Event() {StartHour=new TimeSpan(8,0,0),EndHour=new TimeSpan(12,0,0),Address=new Address() { City="ירושלים",Number=1,Street="jkjk"},Date=new DateTime(2019,04,26),Name="הפגנה"
               ,Ambulances=Ambulances.Where(x=>x.Id==1||x.Id==3||x.Id==2).ToList()},
            };

            Event.ForEach(x => context.Events.Add(x));
            context.SaveChanges();
            var a = context.Events.FirstOrDefault();
            #endregion
            #region AssignmentFoEevent

            var AssignmentFoEevent = new List<AssignmentForEvent>()
            {
                new AssignmentForEvent() {IdAmbulance=1,IdVolunteer=4,IdTraining=1 ,IdEvent=1},
                new AssignmentForEvent() {IdAmbulance=1,IdVolunteer=5,IdTraining=1 ,IdEvent=1}
            };
            AssignmentFoEevent.ForEach(x => context.AssignmentsForEvent.Add(x));
            context.SaveChanges();
            #endregion
        }
        public StockInitilizer()
        {
        }
    }
}
