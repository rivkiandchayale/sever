﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DAL
{
    public class Context : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<ActuallyEquipment> ActuallyEquipments { get; set; }
        public DbSet<Ambulance> Ambulances { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Equipment> Equipments { get; set; }
        public DbSet<EquipmentForAmbulance> EquipmentsForAmbulances { get; set; }
        public DbSet<EquipmentForVolunteer> EquipmentsForVolunteers { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Shift> Shifts { get; set; }
        public DbSet<ShiftKind> ShiftKinds { get; set; }
        public DbSet<Staff> Staffes { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<Volunteer> Volunteers { get; set; }
        public DbSet<AssignmentForShift> AssignmentsForShift { get; set; }
        public DbSet<AssignmentForEvent> AssignmentsForEvent { get; set; }
        public Context() : base("name=Model")
        {
            Database.SetInitializer(new StockInitilizer());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<User>().ToTable("Users");

            modelBuilder.Entity<ActuallyEquipment>().ToTable("ActuallyEquipments");
            modelBuilder.Entity<Ambulance>().ToTable("Ambulances");
            modelBuilder.Entity<Branch>().ToTable("Branches");
            modelBuilder.Entity<City>().ToTable("Cities");
            modelBuilder.Entity<District>().ToTable("Districts");
            modelBuilder.Entity<Equipment>().ToTable("Equipments");
            modelBuilder.Entity<EquipmentForAmbulance>().ToTable("EquipmentsForAmbulances");
            modelBuilder.Entity<EquipmentForVolunteer>().ToTable("EquipmentsForVolunteers");
            modelBuilder.Entity<Event>().ToTable("Events");
            modelBuilder.Entity<Permission>().ToTable("Permissions");
            modelBuilder.Entity<Shift>().ToTable("Shifts");
            modelBuilder.Entity<ShiftKind>().ToTable("ShiftKinds");
            modelBuilder.Entity<Staff>().ToTable("Staffes");
            modelBuilder.Entity<Training>().ToTable("Trainings");
            modelBuilder.Entity<AssignmentForShift>().ToTable("AssignmentForShifts");
            modelBuilder.Entity<AssignmentForEvent>().ToTable("AssignmentForEvents");
        }
    }
}
