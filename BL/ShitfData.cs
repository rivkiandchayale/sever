﻿using DAL;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.DTO;
using BL.Exceptions;

namespace BL
{
    public static class ShitfData
    {
        /// <summary>
        /// קבלת נתונים להצגת שיבוץ למשמרת
        /// </summary>
        /// <param name="id">קוד מתנדב</param>
        /// <returns></returns>
        public static ShiftDataModel GetShiftData(string id)
        {
            try
            {
                ShiftDataModel model = new ShiftDataModel();
                using (Context context = new Context())
                {
                    model.Cities = context.Cities.Select(x => new IdNameModel
                    {
                        Id = x.Id,
                        Name = x.Name
                    }).ToList();
                    model.Shifts = context.ShiftKinds.Select(x => new IdNameModel
                    {
                        Id = x.Id,
                        Name = x.Name
                    }).ToList();

                    Volunteer volunteer = context.Volunteers.FirstOrDefault(x => x.Id.ToString() == id);
                    if (volunteer.Permissions.Count > 0)
                    {
                        if (volunteer.Permissions.Any(x => x.Name == ePermissionNames.מנהל_ארצי || x.Name == ePermissionNames.מנהל_מערכת))
                        {
                            model.Volunteers = context.Volunteers.Select(x => new VolunteerToDropDown
                            {
                                Id = x.Id,
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                PrivateId = x.PrivateId
                            }).ToList();
                        }
                        //todo: מנהל אזורי ואחראי שיבוץ אזורי
                        else
                        {//מביא את כל מי שבסניף שלו 
                            if (volunteer.Permissions.Any(x => x.Name == ePermissionNames.ראש_סניף))
                            {
                                context.Volunteers.Where(x => x.Staff.Branch.IdManage.ToString() == id).ToList();
                            }
                        }
                        //שלא יביא את המתנדב עצמו
                        model.Volunteers = model.Volunteers.Where(x => x.Id.ToString() != id).ToList();
                    }
                    #region
                    //model.Trainings = context.Volunteers.Where(x => x.Id == userId).Select(x => x.Trainings).FirstOrDefault().Select(x => new IdNameModel
                    //{
                    //    Id = x.Id,
                    //    Name = x.Name
                    //}).ToList();
                    #endregion
                }
                return model;
            }
            catch (Exception)
            {
                throw new Exception("שגיאה בקבלת הנתונים");
            }
        }

        /// <summary>
        /// יצירת משמרת חדשה לאמבולנס לפי הנתונים שנבחרו ושיבוץ המתנדב
        /// </summary>
        /// <param name="shiftFormModel">נתונים לפתחית משמרת חדשה ושיבוץ עליה</param>
        /// <returns></returns>
        public static int CreateNewShift(ShiftFormModel shiftFormModel)
        {
            using (Context context = new Context())
            {

                var kindShift = context.ShiftKinds.FirstOrDefault(s => s.Id == shiftFormModel.kindShift).Name;
                string city = context.Cities.FirstOrDefault(c => c.Id == shiftFormModel.City).Name;
                var training = context.Trainings.FirstOrDefault(t => t.Id == shiftFormModel.training).Name.ToString();
                var selectDate = shiftFormModel.Date.Date;

               
                try
                {               
                    //todo : בדיקת שיבוץ באירוע במקביל
                    //אם משובץ כבר במקביל
                    if (context.Volunteers.FirstOrDefault(x => x.Id == shiftFormModel.idUser).AssignmentForShift.Any(y => y.Shift.Date.Equals(selectDate) && y.Shift.IdShiftKind == shiftFormModel.kindShift))
                    {
                        throw new Exception(kindShift + "  הינך משובץ כבר בתאריך זה ב");
                    }                   

                    var ambulances = context.Ambulances.Where(am => am.IdCity == shiftFormModel.City && am.ICU == shiftFormModel.ICU).ToList();
                    foreach (var ambulance in ambulances)
                    {
                        if (!ambulance.Shifts.Any(s => s.Date.Equals(shiftFormModel.Date) && s.IdShiftKind == shiftFormModel.kindShift))
                        {
                            if (!ambulance.Events.Any(e => e.Date.Date.Equals(shiftFormModel.Date)))
                            {
                                var shift = new Shift()
                                {
                                    Date = shiftFormModel.Date,
                                    IdAmbulance = ambulance.Id,
                                    IdShiftKind = shiftFormModel.kindShift,
                                    AssignmentForShift = new List<AssignmentForShift>()
                                };
                                var assignmentForShift = new AssignmentForShift()
                                {
                                    IdTraining = shiftFormModel.training,
                                    IdVolunteer = shiftFormModel.idUser
                                };
                                shift.AssignmentForShift.Add(assignmentForShift);
                                context.Shifts.Add(shift);
                                context.SaveChanges();
                                var email = context.Volunteers.FirstOrDefault(v => v.Id == shiftFormModel.idUser).Email;
                                string body = String.Format("פתחת משמרת חדשה לאמבולנס {0} משמרת {1} בתאריך {2} שובצת למשמרת זו עם הכשרה {3} תודה! ", ambulance.CodeAmbulance, kindShift, shiftFormModel.Date, training);
                                GeneralFunctionData.SendEmail(email, "אישור פתיחת משמרת", body);
                                return ambulance.CodeAmbulance;
                            }
                        }
                    }
                    string message = String.Format("לא נמצא אמבולנס פנוי בתאריך {0} במשמרת {1} בעיר {2} נסה נתונים אחרים", shiftFormModel.Date.Date, kindShift, city);
                    throw new Exception(message);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
        /// <summary>
        /// קבלת נתונים לבחירת משמרת חדשה ושיבוץ עליה
        /// </summary>
        /// <param name="id">קוד מתנדב כדי להביא את ההכשרות שלו</param>
        /// <returns></returns>
        public static DataOfNewShiftModel GetDataOfNewShift(int id)
        {
            var data = new DataOfNewShiftModel();
            using (Context context = new Context())
            {
                try
                {

                    data.kindShifts = context.ShiftKinds.Select(x => new IdNameModel
                    {
                        Id = x.Id,
                        Name = x.Name
                    }).ToList();
                    data.MyTrainings = context.Volunteers.FirstOrDefault(v => v.Id == id).Trainings.Select(t => new MyTrainingModel
                    {
                        Id = t.Id,
                        Name = t.Name.ToString(),
                        IsICU = TrainingData.TrainingIsICU(t.Id)
                    }).ToList();
                }
                catch (Exception)
                {
                    throw new Exception("תקלה בקבלת הנתונים מהשרת");
                }
                return data;
            }
        }
        /// <summary>
        /// מחיקת מתנדב משיבוץ למשמרת
        /// </summary>
        /// <param name="id">קוד מתנדב</param>
        /// <param name="idShift">מאיזה משמרת לבטל שיבוץ</param>
        public static void deleteVlonteerFromShift(int id, int idShift)
        {
            using (Context context = new Context())
            {
                try
                {
                    var shift = context.AssignmentsForShift.FirstOrDefault(x => x.IdShift == idShift && id == x.IdVolunteer);
                    //שמירת הנתונים לפני שימחקו כדי לשלוח אישור
                    var sName = shift.Shift.ShiftKind.Name;
                    var sDate = shift.Shift.Date;
                    var codeAmbulance = shift.Shift.Ambulance.CodeAmbulance;
                    context.AssignmentsForShift.Remove(shift);
                    context.SaveChanges();
                    var email = context.Volunteers.FirstOrDefault(v => v.Id == id).Email;
                    string body = String.Format("הוסרת בהצלחה משיבוץ למשמרת {0} ;אמבולנס מספר{1} בתאריך {2} תודה", sName, codeAmbulance, sDate);
                    GeneralFunctionData.SendEmail(email, "אישור ביטול שיבוץ למשמרת", body);
                }
                catch (Exception)
                {
                    throw new Exception("לא ניתן למחוק מתנדב זה ממשמרת זו");
                }

            }
        }
        /// <summary>
        /// קבלת רשימה של משמרות אם נשלח קוד מתנדב משמרות שהוא משובץ בהם ועוד לא עבר התאריך
        /// </summary>
        /// <param name="idVolunteer">קוד מתנדב</param>
        /// <returns></returns>
        public static List<ShiftModel> GetListOfShift(int idVolunteer)
        {
            List<Shift> shiftDB;
            var Shifts = new List<ShiftModel>();
            using (Context context = new Context())
            {
                try
                {
                    if (idVolunteer == 0)//אם זה מנהל ויש להציג את כל המשמרות
                    {
                        shiftDB = context.Shifts.ToList();
                    }
                    else//אם זה מתנדב לראות את כל המשמרות שבהם הוא נמצא
                    {
                        var date = DateTime.Now.Date;
                        shiftDB = context.Shifts.Where(x => x.AssignmentForShift.Any(y => y.IdVolunteer == idVolunteer) && x.Date >= date).ToList();
                    }
                    Shifts = shiftDB.Select(s => new ShiftModel
                    {
                        Id = s.Id,
                        Date = s.Date,
                        City = new IdNameModel() { Id = s.Ambulance.City.Id, Name = s.Ambulance.City.Name },
                        KindOfShift = new IdNameModel() { Name = s.ShiftKind.Name, Id = s.ShiftKind.Id },
                        numOfAmbulance = s.Ambulance.CodeAmbulance,
                        IsICU = s.Ambulance.ICU,
                        volunteers = s.AssignmentForShift.Select(ass => new VounteerInAssignment
                        {
                            Id = (int)ass.IdVolunteer,
                            FirstName = ass.Volunteer.FirstName,
                            LastName = ass.Volunteer.LastName,
                            PrivateId = ass.Volunteer.PrivateId,
                            IsMine = ass.Volunteer.Id == idVolunteer ? true : false,
                            training = new IdNameModel() { Name = ass.Trainig.Name.ToString(), Id = ass.Trainig.Id }
                        }).ToList()

                    }).ToList();
                    return Shifts;
                }
                catch (Exception)
                {

                    throw new Exception("שגיאה בקבלת הנתונים");
                }
            }
        }
        /// <summary>
        ///פונקציה שמשבצת את המתנדב למשמרת שבחר עם ההכשרה שבחר
        /// </summary>
        /// <param name="idVolunteer"></param>
        /// <param name="choose"></param>
        public static void AssigmentVolunteerToShift(int idVolunteer, ChoosenAssigmentModel choose)
        {
            using (Context context = new Context())
            {
                try
                {
                    //מציאת המשמרת שבחר להשתבץ
                    var shift = context.Shifts.FirstOrDefault(x => x.Id == choose.IdShift);
                    var selectDate = shift.Date.Date;
                    //מתנדב זה כבר משובץ באותו תאריך ובאותו סוג משמרת
                    if (context.Volunteers.FirstOrDefault(x => x.Id == idVolunteer).AssignmentForShift.Any(y => y.Shift.Date.Equals(selectDate) && y.Shift.IdShiftKind == shift.IdShiftKind))
                    {
                        throw new NotPossibleException(shift.ShiftKind.Name + "  הינך משובץ כבר בתאריך זה במשמרת");
                    }
                    //מתנדב זה כבר משובץ באותו תאריך לאירוע  בשעות מקבילות  
                    if (context.Volunteers.FirstOrDefault(x => x.Id == idVolunteer).AssignmentForEvents.Any(y => y.Event.Date.Equals(selectDate) && GeneralFunctionData.equalTimeSpans(y.Event.StartHour, shift.ShiftKind.StartHour, y.Event.EndHour, shift.ShiftKind.EndHour)))
                    {
                        throw new NotPossibleException( "הינך משובץ כבר בתאריך זה באירוע אחר בשעות מקבילות");
                    }
                    if (shift.AssignmentForShift.Count == 4)//אם המשמרת התמלאה בינתיים
                    {
                        throw new NotPossibleException("משמרת זו כבר התמלאה ברגעים האחרונים");
                    }
                    if (shift.Ambulance.ICU == true)//אם משתבץ לאמבולנס נטן
                    {
                        if (shift.AssignmentForShift.Any(x => x.IdTraining == choose.IdTraining))// אם לא השתבצו בינתיים להכשרה זו 
                        {
                            throw new TrainingAlreadyExistsException("במשמרת זו השתבץ כבר מתנדב בהכשרה זו");
                        }
                    }
                    else//אם משתבץ לאמבולנס רגיל
                    {
                        if ((choose.IdTraining == context.Trainings.FirstOrDefault(x => x.Name == eTrainigNames.נהג).Id) || (choose.IdTraining == context.Trainings.FirstOrDefault(x => x.Name == eTrainigNames.נהג).Id))// אם משתבץ כנהג
                        {
                            //אם יש נהג כבר משובץ
                            if (shift.AssignmentForShift.Any(x => x.Trainig.Name == eTrainigNames.נהג || x.Trainig.Name == eTrainigNames.נהג_נטן))
                            {
                                throw new TrainingAlreadyExistsException("במשמרת זו משובץ כבר נהג");
                            }
                        }
                        //השבבץ בהכשרה אחרת
                        else
                        {
                            if (choose.IdTraining == context.Trainings.FirstOrDefault(x => x.Name == eTrainigNames.חובש).Id)//אם משתבץ כחובש
                            {
                                if (shift.AssignmentForShift.Count == 3)//אם משובצים כבר 3 מתנדבים בדיקה אם יש נהג
                                {
                                    if (!(shift.AssignmentForShift.Any(x => x.Trainig.Name == eTrainigNames.נהג || x.Trainig.Name == eTrainigNames.נהג_נטן)))
                                    {
                                        throw new TrainingAlreadyExistsException("אינך יכול להשתבץ כיון שחסר נהג");
                                    }
                                }
                            }
                            //אם השתבץ לא כחובש ולא כנהג - כיון שחייב להיות לפחות חובש אחד ונהג אחד בכל משמרת
                            else
                            {
                                if (shift.AssignmentForShift.Count >= 2)//אם משובצים כבר 2 והוא לא חובש או נהג בדיקה אם כבר יש חובש או נהג
                                {
                                    if (!(shift.AssignmentForShift.Any(x => x.Trainig.Name == eTrainigNames.נהג || x.Trainig.Name == eTrainigNames.נהג_נטן)) && !(shift.AssignmentForShift.Any(x => x.Trainig.Name == eTrainigNames.חובש)))
                                    {
                                        throw new TrainingAlreadyExistsException("אינך יכול להשתבץ כיון שחסר נהג או חובש");
                                    }
                                }
                            }
                        }
                    }
                    //אם עבר את כל הבדיקות יצירת שיבוץ חדש למשמרת זו אם ההכשרה שנבחרה והמתנדב שמשובץ
                    var assignment = new AssignmentForShift()
                    {
                        IdShift = choose.IdShift,
                        IdTraining = choose.IdTraining,
                        IdVolunteer = idVolunteer
                    };
                    context.AssignmentsForShift.Add(assignment);
                    context.SaveChanges();
                    //שליחת מייל לאישור
                    var email = context.Volunteers.FirstOrDefault(v => v.Id == idVolunteer).Email;
                    string body = String.Format("השתבצת למשמרת {0},באמבולנס מספר {1},בתאריך {2} בהצלחה ", shift.ShiftKind.Name, shift.Ambulance.CodeAmbulance, shift.Date);
                    GeneralFunctionData.SendEmail(email, "אישור שיבוץ למשמרת", body);
                }
                catch (NotPossibleException e)
                {
                    throw e;
                }
                catch (TrainingAlreadyExistsException e)
                {
                    throw e;
                }
                catch (Exception)
                {
                    throw new Exception("שיבוץ המתנדב לאירוע לא הצליחה");
                }
            }
        }

        /// <summary>
        /// פונקיה שמחפשת משמרות שמתאימות לקרטיריונים שבחר ובונה מערך שמתאים לו לפי ההכשרות שלו
        /// </summary>
        /// <param name="select">אוביקט שמכיל את הקריטריונים שבחר</param>
        /// <returns></returns>
        public static ChoosenShiftModel SearchShift(SelectShift select)
        {
            using (Context context = new Context())
            {
                try
                {
                    ChoosenShiftModel Otef = new ChoosenShiftModel();
                    List<IcuModel> choosenShift = new List<IcuModel>();
                    List<NotIcuModel> NotIcuList = new List<NotIcuModel>();
                    List<Shift> shifts = new List<Shift>();
                    //מציאת המשמרות העונות לקריטריונים שבחר               
                    var selectDate = select.date.Date;
                    if (select.KindShift == 0)
                    {
                        shifts = context.Shifts.Where(x => x.Date.Equals(selectDate)
                        && x.Ambulance.IdCity == select.City).ToList();
                    }
                    else
                    {
                        shifts = context.Shifts.Where(x => x.Date.Equals(selectDate) &&
                        (x.IdShiftKind == select.KindShift)
                        && x.Ambulance.IdCity == select.City).ToList();
                    }
                    //מעבר על כל המשמרות שנשלפו והמרתם לרשימה של משמרות אם שיבוצים
                    foreach (var sh in shifts)
                    {
                        if (!sh.AssignmentForShift.Any(x => x.IdVolunteer == select.User))//אם מתנדב זה עדין לא משובץ במשמרת זו
                        {
                            IcuModel cs = new IcuModel();
                            #region מילוי רשימת אמבולנס נט"ן
                            if (sh.Ambulance.ICU == true)//אמבולנס נטן
                            {
                                if (sh.AssignmentForShift.Count < 4)//אם המשמרת עוד לא מלאה
                                {
                                    // בדיקה על 4 הכשרות לעייל שחייבות להיות בנט"ן 
                                    cs.ChoosenShiftVolunteersModel = new List<ChoosenShiftVolunteerModel>();
                                    var t = sh.AssignmentForShift.Any(p => p.Trainig.Name == eTrainigNames.חובש_als);//אם הכשרה זו קיימת כבר במשמרת זו
                                    //פונקציה שיוצרת הכשרה לשיבוץ לפי קריטריונים
                                    cs.ChoosenShiftVolunteersModel.Add(GeneralFunctionData.AddTrainingToAmbulance(select.User, t, eTrainigNames.חובש_als));

                                    var t1 = sh.AssignmentForShift.Any(p => p.Trainig.Name == eTrainigNames.נהג_נטן);//אם הכשרה זו קיימת כבר במשמרת זו
                                    cs.ChoosenShiftVolunteersModel.Add(GeneralFunctionData.AddTrainingToAmbulance(select.User, t1, eTrainigNames.נהג_נטן));

                                    var t2 = sh.AssignmentForShift.Any(p => p.Trainig.Name == eTrainigNames.פראמדיק);//אם הכשרה זו קיימת כבר במשמרת זו
                                    cs.ChoosenShiftVolunteersModel.Add(GeneralFunctionData.AddTrainingToAmbulance(select.User, t2, eTrainigNames.פראמדיק));

                                    var t3 = sh.AssignmentForShift.Any(p => p.Trainig.Name == eTrainigNames.רופא);//אם הכשרה זו קיימת כבר במשמרת זו
                                    cs.ChoosenShiftVolunteersModel.Add(GeneralFunctionData.AddTrainingToAmbulance(select.User, t3, eTrainigNames.רופא));

                                    ///בדיקה האם יש לפחות הכשרה אחת שהוא יכול להשתבץ איתה
                                    if (cs.ChoosenShiftVolunteersModel.FirstOrDefault(p => p.Possible == true) != null)
                                    {
                                        //אם כן הוספתו למערך המשמרות ומילוי נתונים נוספים בסיסיים
                                        cs.IdShift = sh.Id;
                                        cs.NumOfAmbulance = sh.Ambulance.CodeAmbulance;
                                        cs.Date = select.date;
                                        cs.ShiftKind = sh.ShiftKind.Name;
                                        choosenShift.Add(cs);
                                    }
                                }

                            }
                            #endregion
                            #region מילוי רשימת אמבולנסים רגילים
                            else//אמבולנס רגיל
                            {
                                NotIcuModel notICU = new NotIcuModel();
                                notICU.CanTraining = new List<IdNameModel>();//רשימת הכשרות שיכול להשתבץ
                                notICU.CanNotTraining = new List<IdNameModel>();//רשימת הכשרות שלא יכול להשתבץ
                                //מילוי בהכשרות שהוא לא יכול להשתבץ את כל ההכשרות שקיימות כבר באמבולנס                              
                                notICU.CanNotTraining = sh.AssignmentForShift
                                    .Select(o => new IdNameModel { Name = o.Trainig.Name.ToString(), Id = context.Trainings.FirstOrDefault(x => x.Name == o.Trainig.Name).Id }).ToList();
                                if (notICU.CanNotTraining.Count < 4)//האמבולנס עדין לא מלא
                                {
                                    //מילוי הנתונים של האמבולנס לפי קריטריונים
                                    GeneralFunctionData.FillNotICU(select.User, notICU);
                                    //אם יש לפחות הכשרה אחת שאיתה יוכל להשתבץ
                                    if (notICU.CanTraining.Count > 0)
                                    {
                                        //מילוי פרטים בסיסיים והסופה לרשימה של האמבולנסים
                                        notICU.IdShift = sh.Id;
                                        notICU.Date = select.date;
                                        notICU.NumOfAmbulance = sh.Ambulance.CodeAmbulance;
                                        notICU.ShiftKind = sh.ShiftKind.Name;
                                        NotIcuList.Add(notICU);
                                    }
                                }
                            }
                            #endregion
                        }
                        //הכנסת הרשימות של האמבולנסים לאוביקט כללי
                        Otef.IcuList = choosenShift;
                        Otef.NotIcuList = NotIcuList;
                        Otef.IdVolunteer = select.User;

                    }
                    return Otef;
                }
                catch (Exception)
                {
                    throw new Exception("שגיאה בקבלת המשמרות לפי מתנדב");
                }
            }
        }


    }
}
