﻿using BL.DTO;
using DAL;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BL
{
    public class GeneralData
    {
        /// <summary>
        /// קבלת כל הערים
        /// </summary>
        /// <returns></returns>
        public static List<IdNameModel> GetAllCity()
        {
            List<IdNameModel> Cities;
            using (Context context = new Context())
            {
                try
                {
                    Cities = context.Cities.Select(x => new IdNameModel
                    {
                        Id = x.Id,
                        Name = x.Name

                    }).ToList();
                    return Cities;
                }
                catch (Exception)
                {
                    throw new Exception();
                }
            }

        }
        /// <summary>
        /// קבלת כל סוג המשמרות
        /// </summary>
        /// <returns></returns>
        public static List<IdNameModel> GetAllKindShift()
        {
            List<IdNameModel> KindShift;
            using (Context context = new Context())
            {
                try
                {
                    KindShift = context.ShiftKinds.Select(x => new IdNameModel
                    {
                        Id = x.Id,
                        Name = x.Name + " " + x.StartHour.Hours + "-" + x.EndHour.Hours

                    }).ToList();
                    return KindShift;
                }
                catch (Exception)
                {
                    throw new Exception();
                }
            }
        }
        /// <summary>
        /// קבלת כל ההרשאות
        /// </summary>
        /// <returns></returns>
        public static List<IdNameModel> GetAllPermissions()
        {
            List<IdNameModel> Permissions;
            using (Context context = new Context())
            {
                try
                {
                    Permissions = context.Permissions.Select(x => new IdNameModel
                    {
                        Id = x.Id,
                        Name = x.Name.ToString()

                    }).ToList();
                    return Permissions;
                }
                catch (Exception)
                {
                    throw new Exception();
                }
            }
        }
        /// <summary>
        ///  קבלת כל הצוותות שלכל אחד רשום איזה מחוז וסניף הוא
        /// </summary>
        /// <returns></returns>
        public static List<IdNameModel> GetAllStaffWithBranchAndDistrict()
        {
            List<IdNameModel> staffs;
            using (Context context = new Context())
            {
                try
                {
                    staffs = context.Staffes.Select(x => new IdNameModel()
                    {
                        Id = x.Id,
                        Name = "  צוות: " + x.Name.ToString() + " סניף: " + x.Branch.Name.ToString() + "  מחוז:  " + x.Branch.District.Name.ToString()

                    }).ToList();
                    return staffs;
                }
                catch (Exception)
                {
                    throw new Exception();
                }
            }
        }
        /// <summary>
        /// קבלת כל ההכשרות
        /// </summary>
        /// <returns></returns>
        public static List<IdNameModel> GetAllTrainings()
        {
            List<IdNameModel> Trainings;
            using (Context context = new Context())
            {
                try
                {
                    Trainings = context.Trainings.Select(x => new IdNameModel
                    {
                        Id = x.Id,
                        Name = x.Name.ToString()

                    }).ToList();
                    return Trainings;
                }
                catch (Exception)
                {
                    throw new Exception();
                }
            }
        }
        private static System.Timers.Timer aTimer;
        //public static void timer()
        //{
        //    aTimer = new System.Timers.Timer(10000);           
        //    aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);            
        //    aTimer.Interval = 2000;
        //    aTimer.Enabled = true;

        //}


        //private static void OnTimedEvent(object sender, ElapsedEventArgs e)
        //{            
        //    //GeneralFunctionData.SendEmail();            
        //}
    }
}
