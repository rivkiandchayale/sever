﻿using DAL;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class TrainingData
    {
        
        public static List<Training> GetTrainingOfAmbulance()
        {
            var training = new List<Training>()
            {
                new Training() { }
            };

            return training;
        }
        /// <summary>
        /// בדיקה על הכשרה מסוימת אם היא הכשרה של אמבולנס נט"ן
        /// </summary>
        /// <param name="idTraining">איזה הכשרה לבדוק</param>
        /// <returns></returns>
        public static bool TrainingIsICU(int idTraining)
        {
            eTrainigNames train = eTrainigNames.none;
            using (Context context = new Context())
            {
                train = context.Trainings.FirstOrDefault(t => t.Id == idTraining).Name;
                switch (train)
                {
                    case eTrainigNames.נהג_נטן: return true;
                    case eTrainigNames.חובש_als: return true;
                    case eTrainigNames.רופא: return true;
                    case eTrainigNames.פראמדיק: return true;
                    default: return false;
                }
            }
        }
    }
  
}
