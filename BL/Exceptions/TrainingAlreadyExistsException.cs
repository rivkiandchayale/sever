﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BL.Exceptions
{
    [Serializable]
    public class TrainingAlreadyExistsException:Exception
    {
        public TrainingAlreadyExistsException()
        {
        }

        public TrainingAlreadyExistsException(string message) : base(message)
        {
        }

        public TrainingAlreadyExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected TrainingAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
