﻿using BL;
using BL.Exceptions;
using DAL;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class EventData
    {
        /// <summary>
        /// קבלת רשימה של כל האירועים 
        /// </summary>
        /// <param name="idVolunteer">אם נשלח קוד מתנדב הבאת כל האירעים שהשתבץ בהם ולא עבר התאריך ואם זה 0 קבלת כל האירועים</param>
        /// <returns></returns>
        public static List<EventModel> GetListOfEvents(int idVolunteer)
        {
            var Events = new List<EventModel>();
            List<Event> eventsDB;
            using (Context context = new Context())
            {
                try
                {
                    if (idVolunteer == 0)// אם רוצים את כל האירועים לא לפי מתנדב
                    {
                        eventsDB = context.Events.ToList();
                    }
                    else//אם רוצים רק את האירועים שמתנדב זה התנדב ועוד לא עבר התאריך של האירוע
                    {
                        var date = DateTime.Now.Date;
                        eventsDB = context.Events.Where(x => x.AssignmentForEvents.Any(y => y.IdVolunteer == idVolunteer) == true && x.Date >= date).ToList();
                    }
                    Events = eventsDB.Select(x => new EventModel
                    {
                        Address = x.Address,
                        Date = x.Date,
                        StartTime=new TimeSpanModel() { Hour=x.StartHour.Hours,Minute=x.StartHour.Minutes},
                        EndTime=new TimeSpanModel() { Hour=x.EndHour.Hours,Minute=x.EndHour.Minutes},
                        Id = x.Id,
                        Name = x.Name,
                        Ambulances = x.Ambulances.Select(a => new AmbulanceInEvent
                        {
                            Id = a.Id,
                            City = new IdNameModel() { Name = a.City.Name, Id = a.City.Id },
                            CodeAmbulance = a.CodeAmbulance,
                            ICU = a.ICU,
                            Volunteers = x.AssignmentForEvents.Where(ass => ass.IdAmbulance == a.Id).Select(ass => new VounteerInAssignment
                            {
                                Id = (int)ass.IdVolunteer,
                                FirstName = ass.Volunteer.FirstName,
                                LastName = ass.Volunteer.LastName,
                                PrivateId = ass.Volunteer.PrivateId,
                                IsMine = ass.Volunteer.Id == idVolunteer ? true : false,
                                training = new IdNameModel() { Name = ass.Trainig.Name.ToString(), Id = ass.Trainig.Id }
                            }).ToList(),

                        }).ToList(),
                        #region אם באירוע יש מתנדבים
                        //Volunteers = x.AssignmentForEvents.Where(ass => ass.IdAmbulance == null && ass.IdVolunteer != null).Select(ass => new VounteerInEvent
                        //{
                        //    FirstName = ass.Volunteer.FirstName,
                        //    LastName = ass.Volunteer.LastName,
                        //    PrivateId = ass.Volunteer.PrivateId,
                        //    IsMine=ass.Volunteer.Id==idVolunteer?true:false,
                        //    trainingInEvent = new IdNameModel() { Name = ass.Trainig.Name.ToString(), Id = ass.Trainig.Id }
                        //}).ToList()
                        #endregion

                    }).ToList();
                }
                catch (Exception)
                {
                    throw new Exception();
                }
            }
            return Events;
        }
        /// <summary>
        /// מחיקת אירוע
        /// </summary>
        /// <param name="idEvent"></param>
        public static void DeleteEvent(int idEvent)
        {
            using (var context = new Context())
            {
                var eve = context.Events.FirstOrDefault(x => x.Id == idEvent);
                context.Events.Remove(eve);
            }
        }

        ///מחיקת מתנדב מהשתבצות באירוע        
        public static void deleteVlonteerFromEvent(int idVolunteer, int idEvent)
        {
            using (var context = new Context())
            {
                try
                {
                    var assignmentsForEvent = context.AssignmentsForEvent.Include("Event").FirstOrDefault(x => x.IdVolunteer == idVolunteer && x.IdEvent == idEvent);
                    var evName = assignmentsForEvent.Event.Name;
                    var evDate = assignmentsForEvent.Event.Date;
                    var CodeAmbulance = assignmentsForEvent.Ambulance.CodeAmbulance;
                    context.AssignmentsForEvent.Remove(assignmentsForEvent);
                    context.SaveChanges();
                    var email = context.Volunteers.FirstOrDefault(v => v.Id == idVolunteer).Email;
                    string body = String.Format("הוסרת בהצלחה משיבוץ לאירוע {0} ;אמבולנס מספר{1} בתאריך {2} תודה", evName, CodeAmbulance, evDate);
                    GeneralFunctionData.SendEmail(email, "אישור ביטול שיבוץ לאירוע", body);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
        /// <summary>
        /// חיפוש אירועים לשיבוץ למתנדב ע"פ ההכשרות שלו והמקומות הפנויים באירוע
        /// </summary>
        /// <param name="idVolunteer">מתנדב שעל פיו יבנה השיבוץ</param>
        /// <returns>מחזיר רשימה של אירועים  שבכל אירוע אמבולנסים ובכל אמבולנס ההכשרות שמתאימות לשיבוץ או הכשרות שמשובצים כבר</returns>
        public static List<ChoosenEventModel> SearchEvents(int idVolunteer)
        {
            using (var context = new Context())
            {
                try
                {
                    var choosenEvent = new List<ChoosenEventModel>();
                    var date = DateTime.Now.Date;
                    //קבלת כל האירועים שהעתידיים שמתנדב זה לא משובץ בהם
                    
                    var events = context.Events.Where(x => (x.Date >= date) && !(x.AssignmentForEvents.Any(y => y.IdVolunteer == idVolunteer))).ToList();
                    //מעבר על כל האירועים שנשלפו והמרתם לרשימת האירועים עם שיבוץ מתאים
                    foreach (var eve in events)
                    {
                        //יצירת אירוע חדש והעתקת הנתונים הבסיסיים
                        ChoosenEventModel choosenEventModel = new ChoosenEventModel
                        {
                            Date = eve.Date,
                            Address = eve.Address,
                            Id = eve.Id,
                            Name = eve.Name,
                            EndTime=new TimeSpanModel() { Hour=eve.EndHour.Hours,Minute=eve.EndHour.Minutes},
                            StartTime = new TimeSpanModel() { Hour=eve.StartHour.Hours,Minute=eve.StartHour.Minutes},
                            ChoosenShift = new ChoosenShiftModel()
                            {
                                IcuList = new List<IcuModel>(),
                                NotIcuList = new List<NotIcuModel>()
                            }
                        };
                        foreach (var a in eve.Ambulances)//מעבר על האמבולנסים באירוע זה
                        {
                            if (a.ICU == true)//אם נטן - הוספתו לרשימת הנטן
                            {
                                var cs = new IcuModel();
                                cs.ChoosenShiftVolunteersModel = new List<ChoosenShiftVolunteerModel>();
                                // בדיקה על 4 הכשרות לעייל שחייבות להיות בנט"ן 
                                //אם באמבולנס זה אין את הכשרה הזו עדין
                                var t = eve.AssignmentForEvents.Any(p => p.Trainig.Name == eTrainigNames.חובש_als && p.IdAmbulance == a.Id);
                                cs.ChoosenShiftVolunteersModel.Add(GeneralFunctionData.AddTrainingToAmbulance(idVolunteer, t, eTrainigNames.חובש_als));

                                var t1 = eve.AssignmentForEvents.Any(p => p.Trainig.Name == eTrainigNames.נהג_נטן);
                                cs.ChoosenShiftVolunteersModel.Add(GeneralFunctionData.AddTrainingToAmbulance(idVolunteer, t1, eTrainigNames.נהג_נטן));

                                var t2 = eve.AssignmentForEvents.Any(p => p.Trainig.Name == eTrainigNames.פראמדיק);
                                cs.ChoosenShiftVolunteersModel.Add(GeneralFunctionData.AddTrainingToAmbulance(idVolunteer, t2, eTrainigNames.פראמדיק));

                                var t3 = eve.AssignmentForEvents.Any(p => p.Trainig.Name == eTrainigNames.רופא);
                                cs.ChoosenShiftVolunteersModel.Add(GeneralFunctionData.AddTrainingToAmbulance(idVolunteer, t3, eTrainigNames.רופא));
                                cs.NumOfAmbulance = a.CodeAmbulance;
                                cs.IdAmbulance = a.Id;
                                //הוספת האמבולנס לרשימת האמבולנסים נט"ן
                                choosenEventModel.ChoosenShift.IcuList.Add(cs);
                            }
                            else//אם רגיל - הוספתו לרשימת האמבולנסים הרגילים
                            {
                                NotIcuModel notICU = new NotIcuModel();
                                notICU.CanTraining = new List<IdNameModel>();//רשימת הכשרות שיכול להשתבץ
                                notICU.CanNotTraining = new List<IdNameModel>();//רשימת הכשרות שלא יכול להשתבץ
                                //מילוי בהכשרות שהוא לא יכול להשתבץ את כל ההכשרות שקיימות כבר באמבולנס
                                notICU.CanNotTraining = eve.AssignmentForEvents.Where(x => x.IdAmbulance == a.Id)
                                    .Select(o => new IdNameModel { Name = o.Trainig.Name.ToString(), Id = context.Trainings.FirstOrDefault(x => x.Name == o.Trainig.Name).Id }).ToList();
                                GeneralFunctionData.FillNotICU(idVolunteer, notICU);
                                //מילוי פרטים בסיסיים
                                notICU.NumOfAmbulance = a.CodeAmbulance;
                                notICU.IdAmbulance = a.Id;
                                //הוספת האמבולנס לרשימת האמבולנסים הרגילים
                                choosenEventModel.ChoosenShift.NotIcuList.Add(notICU);

                            }
                        }
                        //הוספת האירוע שנוצר לרשימת האירועים
                        choosenEvent.Add(choosenEventModel);
                    }
                    return choosenEvent;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
        /// <summary>
        /// יצירת אירוע חדש עי המנהל
        /// </summary>
        /// <param name="eventForm">קבלת נתונים לאירוע חדש</param>
        public static void CreateEvent(EventForm eventForm)
        {
            var AmbulanceNotCan = new List<int>();
           var StartHour = new TimeSpan(eventForm.StartTime.Hour, eventForm.StartTime.Minute, 0);
           var EndHour = new TimeSpan(eventForm.EndTime.Hour, eventForm.EndTime.Minute, 0);
            using (var context = new Context())
            {
                try
                {
                    foreach (var item in eventForm.Ambulances)//בדיקה אם יש אמבולנסים שאינם פנויים בתאריך זה ושמירתם במערך
                    {
                        var ambulance = context.Ambulances.FirstOrDefault(x => x.CodeAmbulance == item);
                        if (ambulance != null)//בדיקה שלא נמחק האמבולנס הזה באמצע התהליך
                        {
                            if ((bool)ambulance.Events?.Any(e => e.Date.Date.Equals(eventForm.Date.Date) && GeneralFunctionData.equalTimeSpans(e.StartHour,StartHour, e.EndHour,EndHour)))//אם יש לאמבולנס אירוע אחר בתאריך שנבחר
                            {
                                AmbulanceNotCan.Add(item);
                            }
                            if ((bool)ambulance.Shifts?.Any(s => s.Date.Date.Equals(eventForm.Date.Date) && GeneralFunctionData.equalTimeSpans(s.ShiftKind.StartHour, StartHour, s.ShiftKind.EndHour, EndHour)))//אם יש לאמבולס משמרות שבתאריך שנבחר
                            {
                                AmbulanceNotCan.Add(item);
                            }
                        }
                    }
                    if (AmbulanceNotCan.Count > 0)//אם נמצא כבר לפחות אמבולנס אחד שתפוס בתאריך זה בשעות מקבילות שנבחר
                    {
                        string message = null;
                        AmbulanceNotCan.ForEach(x => message += x.ToString() + ",");
                        throw new Exception("אמבולנסים: " + message + " תפוסים בתאריך הנבחר");
                    }
                    else//אם לא נמצא אמבולנס שמשובץ כבר בתאריך אחר
                    {
                        Event ev = new Event()
                        {
                            Address = eventForm.Address,
                            Date = eventForm.Date,
                            StartHour = StartHour,
                            EndHour= EndHour,
                            Name = eventForm.Name,
                            Ambulances = new List<Ambulance>()
                        };
                        // eventForm.Ambulances.ForEach(x => ev.Ambulances.Add(context.Ambulances.FirstOrDefault(a => a.CodeAmbulance == x)));
                        foreach (var item in eventForm.Ambulances)
                        {
                            var am = context.Ambulances.FirstOrDefault(a => a.CodeAmbulance == item);
                            if (am != null)
                            {
                                ev.Ambulances.Add(am);
                            }
                        }
                        context.Events.Add(ev);
                        context.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
        /// <summary>
        /// קבלת סטטיסטיקה של נתונים על אירעים כמה אירועים היה בכל עיר בתאריכים שנבחרו כדי להשוואת ביניהם 
        /// </summary>
        /// <param name="Date">2 תאירכים שלפיהם רוצים להשוואת כמות אירועים בכל עיר</param>
        /// <returns></returns>
        public static StatisticsEventsModel GetStatisticsOfEventOf2Month(DateModel Date)
        {
            var Model = new StatisticsEventsModel();
            Model.SumOfEventInDate1 = new List<int>();
            Model.SumOfEventInDate2 = new List<int>();
            using (var context = new Context())
            {
                Model.Cities = context.Cities.Select(x => x.Name).ToList();
                foreach (var city in Model.Cities)
                {
                    try
                    {
                        int sum1 = 0, sum2 = 0;
                        foreach (var events in context.Events)
                        {
                            
                            if (events.Date.Month.ToString()+1 == Date.d1.Month.ToString()+1 && events.Address.City == city)
                            {
                                sum1++;
                            }
                            if (events.Date.Month.ToString()+1 == Date.d2.Month.ToString()+1 && events.Address.City == city)
                            {
                                sum2++;
                            }
                        }
                        Model.SumOfEventInDate1.Add(sum1);
                        Model.SumOfEventInDate2.Add(sum2);
                    }
                    // int sum1 = context.Events.Count(x => x.Date.Month.ToString() == data1 && x.Address.City == city);

                    //int sum2 = context.Events.Count(x => x.Date.Month.ToString() == d2.Month.ToString() && x.Address.City == city);
                    // Model.SumOfEventInDate2.Add(sum2);

                    catch (Exception e)
                    {
                        throw new Exception();
                    }

                }
            }
            return Model;
        }

        /// <summary>
        /// פונקציה שבודקת האם השיבוץ של מתנדב זה אפשרי ואם כן משבצת אותו 
        /// </summary>
        /// <param name="idVolunteer">מתנדב לשיבוץ</param>
        /// <param name="choose">אוביקט שמכיל את המידע על השיבוץ:אירוע, הכשרה ואמבולנס</param>
        public static void AssigmentVolunteerToEvent(int idVolunteer, ChoosenAssigmentEventModel choose)
        {
            using (Context context = new Context())
            {
                try
                {
                    var eve = context.Events.FirstOrDefault(x => x.Id == choose.IdEvent);//שליפת האירוע
                    var selectDate = eve.Date.Date;
                    //מתנדב זה כבר משובץ באותו תאריך באירוע אחר
                    if (context.Volunteers.FirstOrDefault(x => x.Id == idVolunteer).AssignmentForEvents.Any(y => y.Event.Date.Date.Equals(selectDate)&&GeneralFunctionData.equalTimeSpans(y.Event.StartHour,eve.StartHour,y.Event.EndHour,eve.EndHour)))
                    {
                        throw new NotPossibleException("הינך משובץ כבר בתאריך זה בשעות מקבילות באירוע אחר");
                    }
                    //מתנדב זה כבר משובץ באותו תאריך במשמרת מקבילה
                    if (context.Volunteers.FirstOrDefault(x => x.Id == idVolunteer).AssignmentForShift.Any(y => y.Shift.Date.Date.Equals(selectDate) && GeneralFunctionData.equalTimeSpans(y.Shift.ShiftKind.StartHour, eve.StartHour, y.Shift.ShiftKind.EndHour, eve.EndHour)))
                    {
                        throw new NotPossibleException("הינך משובץ כבר בתאריך זה בשעות מקבילות במשמרת");
                    }

                    if (eve.AssignmentForEvents.Where(x => x.IdAmbulance == choose.IdAmbulance).ToList().Count == 4)//אם אמבולנס התמלא בינתיים
                    {
                        throw new NotPossibleException("אמבולנס מלא");
                    }
                    if (eve.Ambulances.FirstOrDefault(x => x.Id == choose.IdAmbulance).ICU == true)//אם משתבץ לאמבולנס נטן
                    {
                        if (eve.AssignmentForEvents.Where(x => x.IdAmbulance == choose.IdAmbulance).Any(y => y.IdTraining == choose.IdTraining))// אם השתבצו בינתיים להכשרה זו 
                        {
                            throw new TrainingAlreadyExistsException("באמבולנס זה השתבץ כבר מתנדב בהכשרה זו");
                        }
                    }
                    else//אם משתבץ לאמבולנס רגיל
                    {
                        if ((choose.IdTraining == context.Trainings.FirstOrDefault(x => x.Name == eTrainigNames.נהג).Id) || (choose.IdTraining == context.Trainings.FirstOrDefault(x => x.Name == eTrainigNames.נהג).Id))// אם משתבץ כנהג
                        {
                            //ויש כבר נהג
                            if (eve.AssignmentForEvents.Where(x => x.IdAmbulance == choose.IdAmbulance).Any(x => x.Trainig.Name == eTrainigNames.נהג || x.Trainig.Name == eTrainigNames.נהג_נטן))
                            {
                                throw new TrainingAlreadyExistsException("באמבולנס זה משובץ כבר נהג");
                            }
                        }
                        else
                        {
                            if (choose.IdTraining == context.Trainings.FirstOrDefault(x => x.Name == eTrainigNames.חובש).Id)//אם משתבץ כחובש
                            {
                                if (eve.AssignmentForEvents.Where(x => x.IdAmbulance == choose.IdAmbulance).ToList().Count == 3)//אם משובצים כבר 3 מתנדבים בדיקה אם יש נהג
                                {
                                    if (!(eve.AssignmentForEvents.Where(x => x.IdAmbulance == choose.IdAmbulance).Any(x => x.Trainig.Name == eTrainigNames.נהג || x.Trainig.Name == eTrainigNames.נהג_נטן)))
                                    {
                                        throw new TrainingAlreadyExistsException("אינך יכול להשתבץ כיון שחסר נהג");
                                    }
                                }
                            }
                            //אם השתבץ לא כחובש ולא כנהג - כיון שחייב להיות לפחות חובש אחד ונהג אחד בכל משמרת
                            else
                            {
                                if (eve.AssignmentForEvents.Where(x => x.IdAmbulance == choose.IdAmbulance).ToList().Count >= 2)//אם משובצים כבר 2 והוא לא חובש או נהג בדיקה אם כבר יש חובש או נהג
                                {
                                    if (!(eve.AssignmentForEvents.Where(x => x.IdAmbulance == choose.IdAmbulance).Any(x => x.Trainig.Name == eTrainigNames.נהג || x.Trainig.Name == eTrainigNames.נהג_נטן)) && !(eve.AssignmentForEvents.Where(x => x.IdAmbulance == choose.IdAmbulance).Any(x => x.Trainig.Name == eTrainigNames.חובש)))
                                    {
                                        throw new TrainingAlreadyExistsException("אינך יכול להשתבץ כיון שחסר נהג או חובש");
                                    }
                                }
                            }
                        }
                    }
                    var assignment = new AssignmentForEvent()//אם יכול להשתבץ, הוספתו לשיבוצים
                    {
                        IdTraining = choose.IdTraining,
                        IdAmbulance = choose.IdAmbulance,
                        IdVolunteer = idVolunteer,
                        IdEvent = choose.IdEvent
                    };
                    context.AssignmentsForEvent.Add(assignment);
                    context.SaveChanges();
                    //שליחת מייל לאישור
                    var email = context.Volunteers.FirstOrDefault(v => v.Id == idVolunteer).Email;
                    int codeAmbulance = eve.Ambulances.FirstOrDefault(x => x.Id == choose.IdAmbulance).CodeAmbulance;
                    string body = String.Format("השתבצת לאירוע {0},באמבולנס מספר {1},בתאריך {2} בהצלחה ", eve.Name, codeAmbulance, eve.Date);
                    GeneralFunctionData.SendEmail(email, "אישור שיבוץ לאירוע", body);
                }
                catch (NotPossibleException e)
                {
                    throw e;
                }
                catch (TrainingAlreadyExistsException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    throw new Exception("שיבוץ המתנדב לאירוע לא הצליחה");
                }
            }
        }
    }
}
