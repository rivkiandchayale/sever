﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class AmbulanceModel
    {
        public int Id { get; set; }
        public int CodeAmbulance { get; set; }
        public bool IsActive { get; set; }
        public bool ICU { get; set; }
        public IdNameModel City { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
