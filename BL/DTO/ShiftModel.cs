﻿using BL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
   public class ShiftModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public IdNameModel KindOfShift { get; set; }
        public IdNameModel City { get; set; }
        public List<VounteerInAssignment> volunteers { get; set; }
        public int numOfAmbulance { get; set; }
        public bool IsICU { get; set; }
    }
}
