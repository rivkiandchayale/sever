﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class ChoosenEventModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public Address Address { get; set; }
        public ChoosenShiftModel ChoosenShift { get; set; }
        public TimeSpanModel StartTime { get; set; }
        public TimeSpanModel EndTime { get; set; }
    }
}
