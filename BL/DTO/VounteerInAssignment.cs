﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
   public class VounteerInAssignment
    {
        public int Id { get; set; }
        public int PrivateId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IdNameModel training { get; set; }
        public bool IsMine { get; set; }
    }
}
