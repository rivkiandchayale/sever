﻿using BL.DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class EventModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public DateTime Date { get; set; }
        public Address Address { get; set; }

        public List<AmbulanceInEvent> Ambulances { get; set; }
        public TimeSpanModel StartTime { get; set; }
        public TimeSpanModel EndTime { get; set; }
        //todo:להוסיף שבאירוע יהיה מתנדבים בנפרד
        //public List<VounteerInEvent> Volunteers { get; set; }
    }
}
