﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
   public class VolunteerModel
    {
        public int Id { get; set; }
        public int PrivateId { get; set; }
        public string Tz { get; set; }
        public string FirstName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }       
        public string Email { get; set; }
        public bool IsActive { get; set; }        
        public IdNameModel Staff { get; set; }
        public IdNameModel Branch { get; set; }
        public IdNameModel District { get; set; }
        public List<IdNameModel> Permissions { get; set; }
        public List<IdNameModel> Trainings { get; set; }

    }
}
