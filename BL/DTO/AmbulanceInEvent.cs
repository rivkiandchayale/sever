﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
   public class AmbulanceInEvent
    {
        public int Id { get; set; }
        public int CodeAmbulance { get; set; }
        public bool ICU { get; set; }
        public IdNameModel City { get; set; }
        public List<VounteerInAssignment> Volunteers { get; set; }
    }
}
