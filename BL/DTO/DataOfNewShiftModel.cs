﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
   public class DataOfNewShiftModel
    {
        public List<MyTrainingModel> MyTrainings { get; set; }
        //public List<AmbulanceModel> ambulances { get; set; }
        public List<IdNameModel> kindShifts { get; set; }
    }
}
