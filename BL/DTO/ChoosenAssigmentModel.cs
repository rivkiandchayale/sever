﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
   public class ChoosenAssigmentModel
    {
        public int IdShift { get; set; }
        public int IdTraining { get; set; }
    }
}
