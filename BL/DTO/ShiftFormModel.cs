﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class ShiftFormModel
    {
        public DateTime Date { get; set; }
        public int kindShift { get; set; }        
        public int training { get; set; }
        public int idUser { get; set; }
        public bool ICU { get; set; }
        public int City { get; set; }
        public string time { get; set; }
    }
}
