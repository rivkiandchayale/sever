﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
   public class ShiftDataModel
    {
       public List<IdNameModel> Cities { get; set; }
        public List<IdNameModel> Shifts { get; set; }

        public List<VolunteerToDropDown> Volunteers { get; set; }
        //public List<IdNameModel> Trainings { get; set; }
    }
}
