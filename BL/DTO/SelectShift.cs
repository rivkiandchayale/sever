﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class SelectShift
    {
        public int KindShift { get; set; }
        public int City { get; set; }
        public int User { get; set; }
        public DateTime date { get; set; }
        public string time { get; set; }

    }
}
