﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace BL.DTO
{
   public class UserModel
    {
        public int Id { get; set; }
        public bool IsVolunteer { get; set; }
        public int PrivateId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<IdNameModel> Permissions { get; set; }
    }
}
