﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class StatisticsEventsModel
    {
        public List<string> Cities { get; set; }
        public List<int> SumOfEventInDate1 { get; set; }
        public List<int> SumOfEventInDate2 { get; set; }
    }
}
