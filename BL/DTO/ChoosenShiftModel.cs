﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class ChoosenShiftModel
    {
        public List<IcuModel> IcuList = new List<IcuModel>();
        public List<NotIcuModel> NotIcuList = new List<NotIcuModel>();
        public int IdVolunteer { get; set; }//קוד של המתנדב שמשובץ כרגע
    }
}
