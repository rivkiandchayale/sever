﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
   public class EventForm
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public List<int> Ambulances { get; set; }
        public Address Address { get; set; }
        public TrainingCount trainingCount { get; set; }
        public TimeSpanModel StartTime { get; set; }
        public TimeSpanModel EndTime { get; set; }
    }

    //todo: traing edit ambulance-bind function-work chack-ambulance-תפוס

}
