﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
   public class ChoosenAssigmentEventModel
    {
        public int IdAmbulance { get; set; }
        public int IdTraining { get; set; }
        public int IdEvent { get; set; }
    }
}
