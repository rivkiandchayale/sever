﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace BL.DTO
{
    public class ChoosenShiftVolunteerModel
    {
        public IdNameModel Training { get; set; }
        public bool IsExist { get; set; }
        public bool Possible { get; set; }
    }
}
