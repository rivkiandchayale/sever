﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class IcuModel
    {
        public int IdShift { get; set; }
        public int NumOfAmbulance { get; set; }
        public int IdAmbulance { get; set; }
        public DateTime Date { get; set; }
        public string ShiftKind { get; set; }
        public List<ChoosenShiftVolunteerModel> ChoosenShiftVolunteersModel { get; set; }

    }
}
