﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class NotIcuModel
    {
        public int IdShift { get; set; }//קוד משמרת
        public int NumOfAmbulance { get; set; }
        public int IdAmbulance { get; set; }
        public string ShiftKind { get; set; }
        public DateTime Date { get; set; }//תאריך
        public List<IdNameModel> CanTraining;//רשימת הכשרות פנויות
        public List<IdNameModel> CanNotTraining;//רשימת הכשרות לא פנויות

    }
}
