﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
   public class AmbulancesPerCity
    {
        public string City { get; set; }
        public List<AmbulanceModel> Ambulances { get; set; }
    }
}
