﻿using BL.DTO;
using DAL;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class GeneralFunctionData
    {
        /// <summary>
        /// הוספת הכשרה לאמבולנס נט"ן לפי קריטריונים שנשלחו
        /// </summary>
        /// <param name="idVolunteer">קוד מתנדב</param>
        /// <param name="isExist">האם ההכשרה קיימת באמבולנס</param>
        /// <param name="name">שם ההכשרה לשיבוץ</param>
        /// <returns></returns>
        public static ChoosenShiftVolunteerModel AddTrainingToAmbulance(int idVolunteer, bool isExist, eTrainigNames name)
        {
            using (Context context = new Context())
            {
                //יצירת אוביקט חדש של הכשרה באמבולנס
                ChoosenShiftVolunteerModel Choosen = new ChoosenShiftVolunteerModel()
                {
                    Training = new IdNameModel
                    {
                        Name = name.ToString(),
                        Id = context.Trainings.FirstOrDefault(x => x.Name == name).Id
                    },
                    //אם ההכשרה קיימת כבר באמבולנס
                    IsExist = isExist,
                    //אם מתנדב זה יכול להתנדב בהכשרה זו - יש לו הכשרה כזו
                    Possible = isExist ? false : context.Volunteers.FirstOrDefault(x => x.Id == idVolunteer).Trainings.Any(y => y.Name == name)
                };
                return Choosen;
            }
        }
        /// <summary>
        /// מילוי רשימת ההכשרות שמתנדב יכול להתנדב לפי פניות באמבולנס והכשרות של מתנדב
        /// </summary>
        /// <param name="idVolunteer">קוד אמבולנס</param>
        /// <param name="notICU">מודל של אמבולנס רגיל למילוי פרטים</param>
        public static void FillNotICU(int idVolunteer, NotIcuModel notICU)
        {
            using (Context context = new Context())
            {
                //שליפת ההכשרות שקיימות למתנדב זה
                var training = context.Volunteers.FirstOrDefault(x => x.Id == idVolunteer).Trainings.Select(l => l.Name);
                //מעבר על כל ההכשרות 
                foreach (var item in training)
                {
                    //אם ההכשרה שלו היא נהג/נהג נטן
                    if (item == eTrainigNames.נהג || item == eTrainigNames.נהג_נטן)
                    {
                        //אם באמבולנס זה לא משובץ כבר נהג או נהג נטן
                        if (notICU.CanNotTraining.Any(p => p.Name == eTrainigNames.נהג.ToString() || p.Name == eTrainigNames.נהג_נטן.ToString()) == false)
                        {
                            //הוספה להכשרות שהוא יכול להשתבץ את הכשרה זו(נהג)
                            notICU.CanTraining.Add(new IdNameModel
                            {
                                Name = item.ToString(),
                                Id = context.Trainings.FirstOrDefault(x => x.Name == item).Id
                            });
                        }
                    }
                    //הכשרה שונה
                    else
                    {
                        //אם כבר קיים באמבולנס נהג או נהג נט"ן כיון שחייב שיהיה נהג אחד לפחות
                        if (notICU.CanNotTraining.Any(p => p.Name == eTrainigNames.נהג.ToString() || p.Name == eTrainigNames.נהג_נטן.ToString()) == true)
                        {
                            //הוספת ההכשרה לרשימת ההכשרות שיכול להשתבץ
                            notICU.CanTraining.Add(new IdNameModel
                            {
                                Name = item.ToString(),
                                Id = context.Trainings.FirstOrDefault(x => x.Name == item).Id
                            });
                        }

                        //אם לא קיים עדין נהג באמבולנס
                        else
                        {
                            //אם מספר ההכשרות קטן משלוש ניתן לשבץ הכשרה אחרת כיון שנשאר מקום פנוי לאמבולנס
                            if (notICU.CanNotTraining.Count < 3)
                                //הוספה
                                notICU.CanTraining.Add(new IdNameModel
                                {
                                    Name = item.ToString(),
                                    Id = context.Trainings.FirstOrDefault(x => x.Name == item).Id
                                });
                        }
                    }
                }
            }
        }
        public static void SendEmail(string addressTo, string Subject, string body)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                message.From = new MailAddress("unitedhazala@gmail.com", "איחוד הצלה - מערכת שיבוץ מתנדבים");
                message.To.Add("9197co@gmail.com");
                message.Subject = Subject;
                message.Body = body;
                SmtpServer.Port = 25;
                SmtpServer.Credentials = new System.Net.NetworkCredential("unitedhazala@gmail.com", "co208556050");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(message);
            }
            catch (Exception e)
            {

            }
        }
        //השוואה האם אירועים או משמרות מקבילים באותם שעות
        public static bool equalTimeSpans(TimeSpan t1Start,TimeSpan t2Start, TimeSpan t1End, TimeSpan t2End)
        {
            if(t1Start>t2End&&t1End<t2Start)
            {
                return false;
            }
            return true;
        }
    }
}
