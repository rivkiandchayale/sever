﻿using BL.DTO;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace BL
{
    public class AmbulanceData
    {
        //קבלת כל האמבולנסים
        public static List<AmbulanceModel> GetListOfAmbulances()
        {
            var anbulances = new List<AmbulanceModel>();
            using (Context contect = new Context())
            {
                try
                {
                    anbulances = contect.Ambulances.Select(x => new AmbulanceModel
                    {
                        Id = x.Id,
                        CodeAmbulance = x.CodeAmbulance,
                        ICU = x.ICU,
                        City = new IdNameModel() { Name = x.City.Name, Id = x.City.Id },
                        IsActive = x.IsActive,
                        Password = x.Password,
                        UserName = x.UserName

                    }).ToList();
                }
                catch (Exception)
                {
                    throw new Exception();
                }
            }
            return anbulances;

        }
        /// <summary>
        /// קבלת רשימה של אמבולנסים מחלוקים לפי עיר 
        /// </summary>
        /// <returns></returns>
        public static List<AmbulancesPerCity> GetAmbulancesPerCity()
        {
            var list = new List<AmbulancesPerCity>();
            using (Context context = new Context())
            {
                var cities = context.Cities.ToList();
                foreach (var item in cities)
                {
                    AmbulancesPerCity am = new AmbulancesPerCity()
                    {
                        City = item.Name,
                        Ambulances = context.Ambulances.Where(x => x.City.Name == item.Name && x.IsActive == true)
                        .Select(c => new AmbulanceModel
                        {
                            CodeAmbulance = c.CodeAmbulance,
                            Id = c.Id,
                            ICU = c.ICU
                        }).ToList()

                    };
                    if (am.Ambulances.Count > 0)
                        list.Add(am);
                }
                return list;
            }
        }
        /// <summary>
        /// שולף את כל האמבולנס הפעילים
        /// </summary>
        /// <returns></returns>
        public static List<AmbulanceModel> GetAllActiveAmbulances()
        {
            var ambulances = new List<AmbulanceModel>();
            using (Context context = new Context())
            {
                try
                {
                    ambulances = context.Ambulances.Where(a => a.IsActive == true).Select(x => new AmbulanceModel
                    {
                        Id = x.Id,
                        ICU = x.ICU,
                        CodeAmbulance = x.CodeAmbulance,
                        City = new IdNameModel { Id = x.City.Id, Name = x.City.Name }
                    }).ToList();
                }
                catch (Exception)
                {
                    throw new Exception();
                }
            }
            return ambulances;
        }
        /// <summary>
        /// יצירת אמבולנס חדש
        /// </summary>
        /// <param name="ambulance">פרטי אמבולנס חדש</param>
        /// <returns></returns>
        public static bool CreateNewAmbulance(Ambulance ambulance)
        {
            //Ambulance ambulance=null;
            using (Context context = new Context())
            {
                try
                {
                    if(context.Users.Any(x=>x.Password==ambulance.Password))
                    {
                        throw new Exception("סיסמה קיימת במערכת בחר סיסמה אחרת");
                    }
                    if(context.Ambulances.Any(x=>x.CodeAmbulance==ambulance.CodeAmbulance))
                    {
                        throw new Exception("קוד אמבולנס קיים");
                    }
                    //TODO:ולא אוביקט של עירID אוביקט שיכיל רק  
                    ambulance.IdCity = ambulance.City.Id;
                    ambulance.City = null;
                    context.Ambulances.Add(ambulance);
                    context.SaveChanges();
                    var a = context.Ambulances.ToList();
                    return true;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// עדכון פרטי אמבולנס
        /// </summary>
        /// <param name="ambulance">פרטי אמבולנס מעודכנים</param>
        /// <returns></returns>
        public static bool updateAmbulance(AmbulanceModel ambulance)
        {
            using (Context context = new Context())
            {
                try
                {
                    var am = context.Ambulances.FirstOrDefault(x => x.Id == ambulance.Id);
                    am.ICU = ambulance.ICU;
                    am.IdCity = ambulance.City.Id;
                    am.IsActive = ambulance.IsActive;
                    am.Password = ambulance.Password;
                    am.UserName = ambulance.UserName;
                    context.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    throw new Exception();
                }
            }
        }
        /// <summary>
        /// קבלת אמבולנס לפי קוד
        /// </summary>
        /// <param name="id">קוד</param>
        /// <returns></returns>
        public static AmbulanceModel GetAmbulanceById(int id)
        {
            AmbulanceModel Ambulance;
            using (Context context = new Context())
            {
                try
                {
                    var am = context.Ambulances.FirstOrDefault(x => x.Id == id);
                    Ambulance = new AmbulanceModel()
                    {
                        Id = am.Id,
                        City = new IdNameModel() { Id = am.City.Id, Name = am.City.Name },
                        CodeAmbulance = am.CodeAmbulance,
                        ICU = am.ICU,
                        IsActive = am.IsActive,
                        UserName = am.UserName,
                        Password = am.Password

                    };
                }
                catch (Exception)
                {
                    throw new Exception();
                }

            }
            return Ambulance;
        }
    }
}
