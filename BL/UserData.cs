﻿using BL.DTO;
using DAL;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public static class UserData
    {

        /// <summary>
        /// קבלת המתשמש שניסה לעשות לוגין אם כל הפרטים שנחוצים לצד קליינט
        /// </summary>
        /// <param name="userName">שם מתשמש</param>
        /// <param name="password">סיסמה</param>
        /// <returns></returns>
        public static UserModel GetUserByLogin(string userName, string password)
        {
            using (Context context = new Context())
            {

                try
                {
                    //todo: רק אם המתנדב פעיל
                    UserModel userModel = new UserModel();
                    #region
                    //userModel.Volunteer = context.Volunteers.FirstOrDefault(x => x.UserName == userName && x.Password == password);
                    //if (userModel.Volunteer == null)
                    //{
                    //    userModel.Ambulance = context.Ambulances.FirstOrDefault(x => x.UserName == userName && x.Password == password);
                    //}
                    //return userModel;
                    #endregion
                    User u = context.Users.FirstOrDefault(x => x.UserName == userName && x.Password == password);
                    if (u != null)
                    {
                        userModel.Id = u.Id;
                        var Volunteer = context.Volunteers.FirstOrDefault(x => x.Id == userModel.Id);
                        if(!Volunteer.IsActive)//אם מתנדב פעיל במערכת
                        {
                            return null;
                        }
                        if (Volunteer != null)//אם מתנדב נמצא ופעיל
                        {
                            userModel.IsVolunteer = true;
                            userModel.FirstName = Volunteer.FirstName;
                            userModel.LastName = Volunteer.LastName;
                            userModel.PrivateId = Volunteer.PrivateId;
                            userModel.Permissions = Volunteer.Permissions.Select(p => new IdNameModel
                            {
                                Id = p.Id,
                                Name = p.Name.ToString()
                            }).ToList();
                        }
                        return userModel;
                    }
                    return null;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }

        }
        /// <summary>
        /// עדכון פרטי מתנדב במערכת
        /// </summary>
        /// <param name="volunteer">פרטי מתנדב חדשים</param>
        public static void UpdateVolunteer(VolunteerModel volunteer)
        {
            using (Context context = new Context())
            {
                try
                {
                    var vo = context.Volunteers.FirstOrDefault(v => v.Id == volunteer.Id);
                    vo.FirstName = volunteer.FirstName;
                    vo.Email = volunteer.Email;
                    vo.IdStaff = volunteer.Staff.Id;
                    vo.IsActive = volunteer.IsActive;
                    vo.LastName = volunteer.LastName;
                    vo.Password = volunteer.Password;
                    vo.UserName = volunteer.UserName;
                    vo.Phone = volunteer.Phone;
                    vo.PrivateId = volunteer.PrivateId;
                    vo.Tz = volunteer.Tz;
                    vo.Permissions.RemoveRange(0, vo.Permissions.Count());
                    context.SaveChanges();
                    var b = context.Trainings.ToList();
                    vo.Trainings.RemoveRange(0, vo.Trainings.Count());
                    context.SaveChanges();
                    var c = context.Trainings.ToList();
                    volunteer.Trainings.ForEach(x => vo.Trainings.Add(context.Trainings.FirstOrDefault(t => t.Id == x.Id)));
                    volunteer.Permissions.ForEach(x => vo.Permissions.Add(context.Permissions.FirstOrDefault(p => p.Id == x.Id)));
                    context.SaveChanges();
                }
                catch (Exception e)
                {

                    throw new Exception();
                }
            }
        }
        /// <summary>
        /// קבלת המתנדבים שתחת מתנדב זה אם יש לו הכשרה 
        /// </summary>
        /// <param name="id">קוד מתנדב</param>
        /// <returns>רשימת מתנדבים</returns>
        public static List<VolunteerToDropDown> GetVolunteersOfThisUser(int id)
        {
            using (Context context = new Context())
            {
                var Volunteers = new List<VolunteerToDropDown>();
                try
                {
                    Volunteer volunteer = context.Volunteers.FirstOrDefault(x => x.Id == id);
                    if (volunteer.Permissions.Count > 0)
                    {
                        if (volunteer.Permissions.Any(x => x.Name == ePermissionNames.מנהל_ארצי || x.Name == ePermissionNames.מנהל_מערכת))
                        {
                            Volunteers = context.Volunteers.Select(x => new VolunteerToDropDown
                            {
                                Id = x.Id,
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                PrivateId = x.PrivateId
                            }).ToList();
                        }
                        //todo: מנהל אזורי ואחראי שיבוץ אזורי
                        else
                        {//מביא את כל מי שבסניף שלו 
                            if (volunteer.Permissions.Any(x => x.Name == ePermissionNames.ראש_סניף))
                            {
                                context.Volunteers.Where(x => x.Staff.Branch.IdManage == id).ToList();
                            }
                        }
                        //שלא יביא את המתנדב עצמו
                        Volunteers = Volunteers.Where(x => x.Id != id).ToList();
                    }
                    return Volunteers;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
        /// <summary>
        /// יצירת מתנדב חדש
        /// </summary>
        /// <param name="volunteer">פרטי מתנדב חדש</param>
        public static void CreateNewVolunteer(VolunteerModel volunteer)
        {
            using (Context context = new Context())
            {
                try
                {
                    if (context.Volunteers.Any(x => x.PrivateId == volunteer.PrivateId))
                    {
                        throw new InvalidOperationException("פרייבט קיים במערכת");
                    }
                    if (context.Volunteers.Any(x => x.Password == volunteer.Password))
                    {
                        throw new InvalidOperationException("סיסמה קיים במערכת");
                    }
                    var v = new Volunteer()
                    {
                        Email = volunteer.Email,
                        FirstName = volunteer.FirstName,
                        IsActive = volunteer.IsActive,
                        LastName = volunteer.LastName,
                        Phone = volunteer.Phone,
                        PrivateId = volunteer.PrivateId,
                        Tz = volunteer.Tz,
                        IdStaff = volunteer.Staff.Id,
                        Password = volunteer.Password,
                        UserName = volunteer.UserName,
                        Permissions = new List<Permission>(),
                        Trainings = new List<Training>()
                    };
                    if (volunteer.Permissions != null)
                        volunteer.Permissions.ForEach(x => v.Permissions.Add(new Permission() { Id = x.Id, Name = (ePermissionNames)Enum.Parse(typeof(ePermissionNames), x.Name, true) }));
                    if (volunteer.Trainings != null)
                        volunteer.Trainings.ForEach(x => v.Trainings.Add(new Training() { Id = x.Id, Name = (eTrainigNames)Enum.Parse(typeof(eTrainigNames), x.Name, true) }));
                    context.Volunteers.Add(v);
                    context.SaveChanges();
                }
                catch (InvalidOperationException e)
                {
                    throw new Exception(e.Message);
                }
                catch (Exception e)
                {
                    throw new Exception("תקלה בשמירת המתנדב נסה שוב מאוחר יותר");
                }
            }
        }
        /// <summary>
        /// קבלת רשימת המתנדבים
        /// </summary>
        /// <returns>רשימת מתנדבים</returns>
        public static List<VolunteerModel> GetListOfVolunteers()
        {
            var Volunteers = new List<VolunteerModel>();
            using (Context contect = new Context())
            {
                try
                {
                    Volunteers = contect.Volunteers.Select(x => new VolunteerModel
                    {
                        Id = x.Id,
                        LastName = x.LastName,
                        FirstName = x.FirstName,
                        Branch = new IdNameModel() { Name = x.Staff.Branch.Name, Id = x.Staff.Branch.Id },
                        Staff = new IdNameModel() { Name = x.Staff.Name, Id = x.Staff.Id },
                        District = new IdNameModel() { Name = x.Staff.Branch.District.Name, Id = x.Staff.Branch.District.Id },
                        IsActive = x.IsActive

                    }).ToList();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            return Volunteers;
        }
        /// <summary>
        /// קבלת מתנדב על פי קוד
        /// </summary>
        /// <param name="id">קוד מתנדב</param>
        /// <returns></returns>
        public static VolunteerModel GetVolunteerById(int id)
        {
            VolunteerModel volunteerModel;
            var volunteer = new Volunteer();
            using (Context contect = new Context())
            {
                try
                {
                    volunteer = contect.Volunteers.FirstOrDefault(x => x.Id == id);
                    volunteerModel = new VolunteerModel()
                    {
                        Id = volunteer.Id,
                        Email = volunteer.Email,
                        FirstName = volunteer.FirstName,
                        LastName = volunteer.LastName,
                        Tz = volunteer.Tz,
                        Phone = volunteer.Phone,
                        Trainings = volunteer.Trainings.Select(t => new IdNameModel() { Id = t.Id, Name = t.Name.ToString() }).ToList(),
                        Permissions = volunteer.Permissions.Select(t => new IdNameModel() { Id = t.Id, Name = t.Name.ToString() }).ToList(),
                        IsActive = volunteer.IsActive,
                        PrivateId = volunteer.PrivateId,
                        Staff = new IdNameModel() { Id = volunteer.Staff.Id, Name = " צוות" + volunteer.Staff.Name.ToString() + "סניף " + volunteer.Staff.Branch.Name.ToString() + " מחוז" + volunteer.Staff.Branch.District.Name.ToString() },
                        Branch = new IdNameModel() { Id = volunteer.Staff.Branch.Id, Name = volunteer.Staff.Branch.Name.ToString() },
                        District = new IdNameModel() { Id = volunteer.Staff.Branch.District.Id, Name = volunteer.Staff.Branch.District.Name.ToString() },
                        Password = volunteer.Password,
                        UserName = volunteer.UserName
                        //TODO:add field
                    };
                    return volunteerModel;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
    }
}
