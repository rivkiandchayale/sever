﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using BL;
using Entity;
using BL.DTO;

namespace WebApi.Controllers
{
    public class LoginController : ApiController
    {
        //קבלת המתשמש שניסה לעשות לוגין אם כל הפרטים שנחוצים לצד קליינט
        [HttpPost]
        public IHttpActionResult Login(User u)
        {
            try
            {
                UserModel userModel = UserData.GetUserByLogin(u.UserName, u.Password);
                return Ok(userModel);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
              
            }
           
        }
    }
}
