﻿using BL;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BL.DTO;

namespace WebApi.Controllers
{
    public class VolunteerController : ApiController
    {
        //קבלת מתנדב על פי קוד
        public IHttpActionResult GetVolunteerById(int id)
        {
            try
            {
               return Ok(UserData.GetVolunteerById(id));
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        //קבלת רשימת המתנדבים
        public IHttpActionResult GetListOfVolunteers()
        {
            try
            {
                return Ok(UserData.GetListOfVolunteers());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        //יצירת מתנדב חדש
        public IHttpActionResult CreateNewVolunteer(VolunteerModel volunteer)
        {
            try
            {
                UserData.CreateNewVolunteer(volunteer);
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

        }

        //עדכון פרטי מתנדב במערכת
        public IHttpActionResult UpdateVolunteer(VolunteerModel volunteer)
        {
            try
            {
                UserData.UpdateVolunteer(volunteer);
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        //קבלת המתנדבים שתחת מתנדב זה 
        public IHttpActionResult GetVolunteersOfThisUser(int id)
        {
            try
            {
                return Ok(UserData.GetVolunteersOfThisUser(id));
            }
            catch (Exception e)
            {

                return InternalServerError(e);
            }
        }
    }
}
