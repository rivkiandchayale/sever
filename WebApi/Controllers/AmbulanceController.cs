﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BL;
using BL.DTO;
using Entity;

namespace WebApi.Controllers
{
    public class AmbulanceController : ApiController
    {
        //קבלת כל האמבולנסים
        public IHttpActionResult GetListOfAmbulances()
        {
            try
            {
                return Ok(AmbulanceData.GetListOfAmbulances());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        //קבלת אמבולנס לפי קוד
        public IHttpActionResult GetAmbulanceById(int id)
        {
            try
            {
                return Ok(AmbulanceData.GetAmbulanceById(id));
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        //עדכון פרטי אמבולנס
        public IHttpActionResult updateAmbulance(AmbulanceModel ambulance)
        {
            try
            {
                return Ok(AmbulanceData.updateAmbulance(ambulance));
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            };
        }
        //יצירת אמבולנס חדש
        public IHttpActionResult CreateNewAmbulance(Ambulance ambulance)
        {
            try
            {
                return Ok(AmbulanceData.CreateNewAmbulance(ambulance));
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        //קבלת רשימה של אמבולנסים מחלוקים לפי עיר 
        public IHttpActionResult GetAmbulancesPerCity()
        {
            try
            {
                return Ok(AmbulanceData.GetAmbulancesPerCity());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
       
       // שולף את כל האמבולנסים שפעילים       
        public IHttpActionResult GetAllActiveAmbulances()
        {
            try
            {
                return Ok(AmbulanceData.GetAllActiveAmbulances());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
