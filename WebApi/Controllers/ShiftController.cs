﻿using BL;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using BL.DTO;
using System.Globalization;
using BL.Exceptions;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class ShiftController : ApiController
    {
       
        [HttpGet]
        public IHttpActionResult GetShiftData(string id)
        {
            try
            {
                var list = ShitfData.GetShiftData(id);
                return Ok(list);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        //מחפשת משמרת לפי נתונים שבחר
        [HttpPost]
        public IHttpActionResult SearchShift(SelectShift select)
        {
            try
            {
                DateTime myDateTime;
                DateTime.TryParse(select.time, out myDateTime);
                select.date = myDateTime;
                ChoosenShiftModel choosenShift = ShitfData.SearchShift(select);
                return Ok(choosenShift);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        //שיבוץ סופי למשמרת שנבחרה
        [HttpPost]
        public IHttpActionResult AssigmentVolunteerToShift(int id, ChoosenAssigmentModel choose)
        {
            try
            {
                ShitfData.AssigmentVolunteerToShift(id, choose);
                return Ok();
            }
            catch (NotPossibleException e)
            {
                return new NotPossibleActionResult(HttpStatusCode.Conflict, e.Message);
            }
            catch (TrainingAlreadyExistsException e)
            {
                return new TrainingActionResult(HttpStatusCode.Forbidden, e.Message);
            }
            catch (Exception e)
            {
                return InternalServerError(e);

            }
        }

        //קבלת רשימה של משמרות אם נשלח קוד מתנדב משמרות שהוא משובץ בהם ועוד לא עבר התאריך
        [HttpGet]
        public IHttpActionResult GetListOfShift(int id)
        {
            try
            {
                List<ShiftModel> shifts = ShitfData.GetListOfShift(id);
                return Ok(shifts);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        //מחיקת מתנדב משיבוץ למשמרת
        [HttpPost]
        public IHttpActionResult deleteVlonteerFromShift(int id, [FromBody] int idShift)
        {
            try
            {
                ShitfData.deleteVlonteerFromShift(id, idShift);
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        //קבלת נתונים לבחירת משמרת חדשה ושיבוץ עליה
        [HttpGet]
        public IHttpActionResult GetDataOfNewShift(int id)
        {
            try
            {
                DataOfNewShiftModel data = ShitfData.GetDataOfNewShift(id);
                return Ok(data);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpPost]
        //יצירת משמרת חדשה לאמבולנס לפי הנתונים שנבחרו ושיבוץ המתנדב
        public IHttpActionResult CreateNewShift(ShiftFormModel shiftFormModel)
        {
            DateTime myDateTime;
            DateTime.TryParse(shiftFormModel.time, out myDateTime);
            shiftFormModel.Date = myDateTime;
            try
            {
                int codeAmbulance = ShitfData.CreateNewShift(shiftFormModel);
                return Ok(codeAmbulance);
            }
            catch (Exception e)
            {
                //return BadRequest(e.Message);
                return InternalServerError(e);
            }
        }
    }
}
