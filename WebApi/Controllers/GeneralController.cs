﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BL.DTO;
using BL;

namespace WebApi.Controllers
{
    public class GeneralController : ApiController
    {

        //קבלת כל הערים
        [HttpGet]
        public IHttpActionResult GetAllCities()
        {
            try
            {
                return Ok(GeneralData.GetAllCity());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        //קבלת כל סוג המשמרות
        [HttpGet]
        public IHttpActionResult GetAllKindShift()
        {
            try
            {
                return Ok(GeneralData.GetAllKindShift());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
      
        /// קבלת כל הצוותות שלכל אחד רשום איזה מחוז וסניף הוא        
        [HttpGet]
        public IHttpActionResult GetAllStaffWithBranchAndDistrict()
        {
            try
            {
                return Ok(GeneralData.GetAllStaffWithBranchAndDistrict());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        //קבלת כל ההרשאות
        [HttpGet]
        public IHttpActionResult GetAllPermissions()
        {
            try
            {
                return Ok(GeneralData.GetAllPermissions());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        //קבלת כל ההכשרות
        [HttpGet]
        public IHttpActionResult GetAllTrainings()
        {
            try
            {
                return Ok(GeneralData.GetAllTrainings());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
