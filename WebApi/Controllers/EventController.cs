﻿using BL.DTO;
using BL.Exceptions;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{

    public class EventController : ApiController
    {
        // קבלת סטטיסטיקה של נתונים על אירעים כמה אירועים היה בכל עיר בתאריכים שנבחרו כדי להשוואת ביניהם 
        [HttpPost]        
        public IHttpActionResult StatisticsOfEventOf2Month(DateModel date)
        {
            //doto: מגיע תאריך חודש קודם
            try
            {             
                return Ok(EventData.GetStatisticsOfEventOf2Month(date));
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        //קבלת רשימה של כל האירועים
        [HttpGet]
        public IHttpActionResult GetListOfEvents(int id)
        {
            try
            {
                return Ok(EventData.GetListOfEvents(id));
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        //יצירת אירוע חדש עי המנהל
        public IHttpActionResult CreateEvent(EventForm eventForm)
        {
            try
            {
                EventData.CreateEvent(eventForm);
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

        }
        //חיפוש אירועים לשיבוץ למתנדב ע"פ ההכשרות שלו והמקומות הפנויים בבאירוע
        [HttpGet]
        public IHttpActionResult SearchEvents(int id)
        {
            try
            {
                return Ok(EventData.SearchEvents(id));
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        //מחיקת מתנדב משיבוץ באירוע
        [HttpPost]
        public IHttpActionResult deleteVlonteerFromEvent(int id, [FromBody]int idEvent)
        {
            try
            {
                EventData.deleteVlonteerFromEvent(id, idEvent);
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

        }
        //שיבוץ מתנדב לאירוע שבחר עם ההכשרה שבחר
        [HttpPost]
        public IHttpActionResult AssigmentVolunteerToEvent([FromUri]int id, [FromBody]ChoosenAssigmentEventModel choosen)
        {
            try
            {
                EventData.AssigmentVolunteerToEvent(id, choosen);
                return Ok();
            }
            catch (NotPossibleException e)
            {
                return new NotPossibleActionResult(HttpStatusCode.Conflict, e.Message);
            }
            catch (TrainingAlreadyExistsException e)
            {
                return new TrainingActionResult(HttpStatusCode.Forbidden, e.Message);
            }
            //שגיאה של השרת
            catch (Exception e)
            {
                return InternalServerError(e);
            }

        }

        //מחיקת אירוע
        [HttpGet]
        public IHttpActionResult DeleteEvent(int idEvent)
        {
            EventData.DeleteEvent(idEvent);
           return Ok();
        }

    }
}




