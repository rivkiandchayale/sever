﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    //public enum ETrainig
    //{
    //    driver,
    //    cHoves,
    //    doctor
    //}
    public enum EWhomEquipment
    {
        ambulance,
        volunteer
    }
    public enum eTrainigNames
    {
        none,
        נהג_נטן,
        חובש_als,
        פראמדיק,
        רופא,
        חובש,
        נהג
    }
    public enum ePermissionNames
    {
        אחראי_שיבוץ_אזורי,
        ראש_סניף,
        מנהל_אזורי,
        מנהל_ארצי,
        מנהל_אבטחות_אירועים,
        מנהל_מרכז_ציוד_ארצי,
        מנהל_מרכז_ציוד_אזורי,
        מנהל_מערכת

    }
}
