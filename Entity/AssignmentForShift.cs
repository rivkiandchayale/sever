﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class AssignmentForShift
    {
        public int Id { get; set; }
        [ForeignKey("Volunteer")]
        public int? IdVolunteer { get; set; }
        [ForeignKey("Shift")]
        public int? IdShift { get; set; }
        [ForeignKey("Trainig")]
        public int? IdTraining { get; set; }
        public virtual Training Trainig { get; set; }
        public virtual Volunteer Volunteer { get; set; }
        public virtual Shift Shift { get; set; }
    }
}
