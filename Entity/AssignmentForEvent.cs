﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class AssignmentForEvent
    {
        public int Id { get; set; }
        [ForeignKey("Volunteer")]
        public int? IdVolunteer { get; set; }
        [ForeignKey("Event")]
        public int? IdEvent { get; set; }
        [ForeignKey("Trainig")]
        public int? IdTraining { get; set; }
        [ForeignKey("Ambulance")]
        public int? IdAmbulance { get; set; }
        public virtual Training Trainig { get; set; }
        public virtual Volunteer Volunteer { get; set; }
        public virtual Event Event { get; set; }
        public virtual Ambulance Ambulance { get; set; }
    }
}
