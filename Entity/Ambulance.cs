﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    [Table("Ambulances")]
    public class Ambulance : User
    {
        public int CodeAmbulance { get; set; }
        public bool IsActive { get; set; }
        public bool ICU { get; set; }
        public virtual List<Event> Events { get; set; }
        public virtual List<Shift> Shifts { get; set; }
        public virtual List<EquipmentForAmbulance> EquipmentForAmbulance { get; set; }
        [ForeignKey("City")]
        public int? IdCity { get; set; }
        public virtual City City { get; set; }
    }
}
