﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class Branch
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        [ForeignKey("District")]
        public int? IdDistrict { get; set; }

        public virtual District District { get; set; }

        public int? IdManage { get; set; }

        public virtual List<Staff> staffs { get; set; }



    }
}
