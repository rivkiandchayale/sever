﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Event
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Id { get; set; }
        public string Name { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime Date { get; set; }       
        public TimeSpan StartHour { get; set; }       
        public TimeSpan EndHour { get; set; }
        public Address Address { get; set; }
        public virtual List<AssignmentForEvent> AssignmentForEvents { get; set; }
        public virtual List<Ambulance> Ambulances { get; set; }

        public Event()
        {
            Date = new DateTime();
        }

    }
}
