﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ActuallyEquipment
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Id { get; set; }
        [ForeignKey("Equipment")]
        public int? IdEquipment { get; set; }
        public virtual Equipment Equipment { get; set; }
        public int QtyInStandart { get; set; }
        public EWhomEquipment WhomEquipment { get; set; }
    }


}
