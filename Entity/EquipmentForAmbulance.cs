﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class EquipmentForAmbulance
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Id { get; set; }
        [ForeignKey("Ambulance")]
        public int? IdAmbulance { get; set; }
        [ForeignKey("Equipment")]
        public int? IdEquipment { get; set; }
        public int QtyInStock { get; set; }
        public virtual Ambulance Ambulance { get; set; }
        public virtual Equipment Equipment { get; set; }

    }
}
