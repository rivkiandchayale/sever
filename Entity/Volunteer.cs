﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Entity
{
    [Table("Volunteers")]
    public class Volunteer : User
    {

        public int PrivateId { get; set; }
        public string Tz { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("Staff")]
        public int? IdStaff { get; set; }
        public virtual Staff Staff { get; set; }
        public virtual List<Permission> Permissions { get; set; }
        public virtual List<Training> Trainings { get; set; } 
        public virtual List<AssignmentForEvent> AssignmentForEvents { get; set; }
        public virtual List<AssignmentForShift> AssignmentForShift { get; set; }
        public virtual List<EquipmentForVolunteer> EquipmentForVolunteer { get; set; }


    }
}
