﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class EquipmentForVolunteer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int? Id { get; set; }
        [ForeignKey("Volunteer")]
        public int? IdVolunteer { get; set; }
        [ForeignKey("Equipment")]
        public int IdEquipment { get; set; }
        public int QtyInStock { get; set; }
        public virtual Volunteer Volunteer { get; set; }
        public virtual Equipment Equipment { get; set; }
    }
}
