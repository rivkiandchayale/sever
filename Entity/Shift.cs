﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class Shift
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        [ForeignKey("ShiftKind")]
        public int? IdShiftKind { get; set; }
        public virtual ShiftKind ShiftKind { get; set; }
        [ForeignKey("Ambulance")]
        public int? IdAmbulance { get; set; }
        public virtual Ambulance Ambulance { get; set; }

        public virtual List<AssignmentForShift> AssignmentForShift { get; set; }

    }
}
